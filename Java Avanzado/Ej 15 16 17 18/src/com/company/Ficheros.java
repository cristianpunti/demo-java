package com.company;

import java.io.*;

public class Ficheros {
    public static void accederFichero() {
        // Excepción al acceder un fichero
        FileInputStream fptr;
        BufferedReader d;
        String	linea = null;
        try{
            fptr = new FileInputStream("/dev/hola");

            d  = new BufferedReader(new InputStreamReader(fptr));

            do {
                linea = d.readLine();
                if (linea!=null) System.out.println(linea);
            } while (linea != null);
            fptr.close();

        } catch (FileNotFoundException e) {
            System.out.println("Hey, ese archivo no existe!\n");
        }
        catch (IOException e) {
            System.out.println("Error de E/S!\n");
        }
    }

}
