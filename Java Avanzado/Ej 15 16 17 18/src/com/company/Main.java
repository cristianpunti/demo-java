package com.company;

import java.io.*;

import static com.company.Ficheros.accederFichero;
import static com.company.OperacionesAritmeticas.division;
import static com.company.OptionParser.demoCLI;
import static com.company.Usuarios.meterUsuario;
public class Main {

    public static void main(String[] args)  {
	// write your code here

//        En estos ejercicios vas a trabajar sobre las excepciones, tendréis que implementar:
//
//        Arithmetic exception tratando de hacer una división entre dos números, siendo el que divide 0.
//
//        ArrayIndexOutOfBounds con un array de 6 elementos, tratando de alcanzar el noveno elemento.
//
//                FileNotFound tratando de acceder a un fichero que no existe.

//        Implementa Args en uno de los proyectos que se han ido trabajando y aplica las buenas prácticas que se han visto en la sesión 19.
//
//        Identifica las características principales de la arquitectura limpia, indica en que casos se tendría que utilizar la arquitectura limpia y como lo harías.
//

        // Características principales de la arquitectura limpia:

        //  1. Principio de responsabilidad única:  Una clase sólo debe ser responsable de una única cosa.
        //   Con el principio de responsabilidad úncia ya nos conllueba que tengamos una separación entre capas
        //   2. Principio de abierto - cerrado: las entidades de software tienen que estar abiertas para que las podamos
        //    extender pero cerradas a la hora de modificarlas. Derivamos clases mediante clases abstractas
        //   3. Principio de substitución Liskov: un programa tiene que tener una jerarquia de clases fácil de entender y
        // adémas tiene que tener un código reutilizable.
        //   4. Principio de separación de una interfaz: una clase no va a implementar a una interfaz que no necesita.
        //   5. Principio de inversión de dependencias: las dependencias se tienen que extraer y que un módulo de alto
        //   nivel no debe depender de un módulo de bajo nivel

        // La aquitectura límpia tiene sentido usarla en el caso que se esté desarrollando una aplicación de vida
        // media o larga.

        // Para realizar la arquitectura límpia, separaría las clases de manera que cada clase tenga una responsabilidad determindada,
        // derivaría clases en caso que sea necesario, reautalizaría el código en el caso que sea necesario, implementaría las clases
        // sólo en los casos que sea necesario y, por último, haría que los módulos de alto nivel dependan de otros moódulos de alto nivel y lo mismo
        // con los módulos de bajo nivel.
        //
        //        Teniendo en cuenta lo visto en la sesión 21, utiliza un proyecto de los trabajados y límpialo. Si encuentras código duplicado, elimínalo y realiza la minificación de las clases y métodos.

        option (args);


// http://commons.apache.org/cli/


        try {
            division(5, 0);
        } catch (ArithmeticException e){
            System.out.println("Hay una excepción aritmética: " + e.getMessage());
        } catch(IOException e){
            System.out.println("Hay una excepción de IO");
        }

        System.out.println("Hola! Estoy aquí");
        String nombre = "";

        meterUsuario(nombre);


        accederFichero();
        demoCLI(args);

    }








    // Aquest mètode aniria dintre de la classe OptionParser
    private static void option (String[] args) {
        OptionParser optionParser = new OptionParser(args);
        optionParser.registerOption("ArithmeticException");
        optionParser.registerOption("ArrayIndexOutOfBoundsException");
        optionParser.registerOption("FileNotFoundException");

        optionParser.parse();

        String value = optionParser.getOption("FileNotFoundException");
        System.out.println("Solution exercise:\t" + value);
    }





}
