package com.company;

import java.io.IOException;

public class OperacionesAritmeticas {
    public static int division(int a, int b) throws ArithmeticException, IOException {
        int resultado = 0;
        try {
            resultado =  a / b;

            if(b == 1) {
                throw new IOException();
            }
        }catch(ArithmeticException e) {
            throw new ArithmeticException("No se puede dividir por  0");
        }

        return  resultado;

    }
}
