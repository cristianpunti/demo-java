package com.company;

import org.apache.commons.cli.*;

import java.util.HashMap;
//import org.apache.commons.cli.DefaultParser;

public class OptionParser {
    private HashMap<String, String> options = new HashMap();

    private String [] args;

    private OptionParser() {}

    public OptionParser(String[] args) {
        this.args = args;
    }

    public void registerOption(String optionName) {
        String value = "";
        options.put(optionName, value);
    }

    public String getOption(String optionName) {

        return options.get(optionName);
    }

    public void setOptionValue(String optionName, String optionValue) {
        options.replace(optionName, optionValue);
    }

    public void parse(){
        for (int i = 0; i < args.length; i++) {
           String optionName = args[i].replace("-", "");
            if (options.containsKey(optionName)) {
                setOptionValue(optionName, args[i]);
            }
        }

    }

    public static void  demoCLI(String[] args) {

        Options options = new Options();

        Option maxval = new Option("m", "max", true, "Valor máximo");
       maxval.setRequired(true);

       options.addOption(maxval);

       CommandLineParser parser = new CommandLineParser() {
           @Override
           public CommandLine parse(Options options, String[] strings) throws ParseException {
               return null;
           }

           @Override
           public CommandLine parse(Options options, String[] strings, boolean b) throws ParseException {
               return null;
           }
       };
       HelpFormatter helpFormatter = new HelpFormatter();
       helpFormatter.printHelp("cmd", options);
        System.exit(-1);
       CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);

        } catch (NumberFormatException e) {
            System.out.println("Error parsing command line: " + e.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int miMaxVal = Integer.parseInt(cmd.getOptionValue("maxval"));

        System.out.println("Max value: " + miMaxVal);


    }
}