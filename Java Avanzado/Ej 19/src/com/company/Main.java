package com.company;

import java.io.*;

public class Main {

    public static void main(String[] args)  {
	// write your code here

//        En estos ejercicios vas a trabajar sobre las excepciones, tendréis que implementar:
//
//        Arithmetic exception tratando de hacer una división entre dos números, siendo el que divide 0.
//
//        ArrayIndexOutOfBounds con un array de 6 elementos, tratando de alcanzar el noveno elemento.
//
//                FileNotFound tratando de acceder a un fichero que no existe.

        try {
            division(5, 0);
        } catch (ArithmeticException e){
            System.out.println("Hay una excepción aritmética: " + e.getMessage());
        } catch(IOException e){
            System.out.println("Hay una excepción de IO");
        }

        System.out.println("Hola! Estoy aquí");


        Usuarios usuarios = new Usuarios();


        try{
            usuarios.meterUsuario("pepito");
            usuarios.meterUsuario("Pep");
            usuarios.meterUsuario("José");

        } catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Excepción! Se ha excedido del rango del array");
        } finally{
            System.out.println("Finally ");
        }


        // Excepción al acceder un fichero
        FileInputStream fptr;
        BufferedReader d;
        String	linea = null;
        try{
            fptr = new FileInputStream("/dev/hola");

                  d  = new BufferedReader(new InputStreamReader(fptr));

            do {
                linea = d.readLine();
                if (linea!=null) System.out.println(linea);
            } while (linea != null);
          fptr.close();

        } catch (FileNotFoundException e) {
            System.out.println("Hey, ese archivo no existe!\n");
        }
        catch (IOException e) {
            System.out.println("Error de E/S!\n");
        }


    }


    public static int division(int a, int b) throws ArithmeticException, IOException {
       int resultado = 0;
        try {
             resultado =  a / b;

            if(b == 1) {
                throw new IOException();
            }
        }catch(ArithmeticException e) {
            throw new ArithmeticException("No se puede dividir por  0");
        }

        return  resultado;

    }
    
    
}
