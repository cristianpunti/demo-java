package com.company;

public class CocheCombustible extends Coche{

    String gasolina;


    public CocheCombustible(String tipo, int velocidad) {
        super(tipo, velocidad);
        this.tipo = "C";
        this.velocidad = 40;
        this.gasolina = "Si";

    }

    public CocheCombustible() {

    }



    public String getGasolina() {
        return gasolina;
    }

    @Override
    public String toString() {
        return "CocheCombustible{" +

                " gasolina='" + gasolina + '\'' +
                ", tipo='" + tipo + '\'' +
                ", velocidad=" + velocidad +
                '}';
    }
}
