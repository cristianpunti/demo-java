package com.company;

public class CocheElectrico extends Coche {

    String baterias;

    public CocheElectrico(String tipo, int velocidad) {
        super(tipo, velocidad);
        this.tipo = "E";
        this.velocidad = 50;
        this.baterias   = "Si";
    }

    public CocheElectrico() {

    }



    public String getBaterias() {
        return baterias;
    }
    @Override
    public String toString() {
        return "CocheElectrico{" +

                " baterias='" + baterias + '\'' +
                ", tipo='" + tipo + '\'' +
                ", velocidad=" + velocidad +
                '}';
    }

}
