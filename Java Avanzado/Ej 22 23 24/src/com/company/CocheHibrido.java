package com.company;

public class CocheHibrido extends Coche {

    String baterias;
    String gasolina;


    public CocheHibrido(String tipo, int velocidad) {
        super(tipo, velocidad);
        this.tipo = "H";
        this.velocidad = 55;
        this.baterias   = "Si";
        this.gasolina = "Si";
    }

    public CocheHibrido() {

    }



    public String getBaterias() {
        return baterias;
    }

    public String getGasolina() {
        return gasolina;
    }

    @Override
    public String toString() {
        return "CocheHibrido{" +

                " baterias='" + baterias + '\'' +
                " gasolina='" + gasolina + '\'' +
                ", tipo='" + tipo + '\'' +
                ", velocidad=" + velocidad +
                '}';
    }
}