package com.company;


import java.io.StringReader;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

//        Durante estos ejercicios se va a trabajar sobre las letras S, O y L de los principios SOLID, deberás:
//
//        Crea una clase coche aplicándole el principio de responsabilidad única (S).
//
//                Continúa con el código extendiéndolo para aumentar su funcionalidad pero sin modificarlo aplicando el principio abierto/cerrado(O).
//
//                Crea subclases de la clase coche, por ejemplo un coche eléctrico, de combustible y uno híbrido y aplica el principio de substitución de Liskov (L), las subclases deben ser sustituibles por la superclase coche.s ejercicios se va a trabajar sobre las letras S, O y L de los principios SOLID, deberá

        Scanner s = new Scanner(System.in);
        int numero = 0;
        String salir = "";
        do {
            System.out.println("1 (Coche con combustible) - 2 (Coche eléctrico) - 3 (Coche hibrido) - 0 (Coche)"   );
            numero = s.nextInt();


            Coche coche = new Coche();

            coche.tipo = coche.getTipoVehiculo();
            coche.velocidad = coche.getVelocidad();

            CocheCombustible cc;
            CocheElectrico ce;
            CocheHibrido ch;



            if (numero == 1) {

                cc = new CocheCombustible(coche.tipo, coche.velocidad);
                System.out.println(cc);
            } else if (numero == 2) {
                ce = new CocheElectrico(coche.tipo, coche.velocidad);
                System.out.println(ce);
            } else if (numero == 3) {
                ch = new CocheHibrido(coche.tipo, coche.velocidad);
                System.out.println(ch);
            } else if (numero == 4) {
                coche = new Coche(coche.tipo, coche.velocidad);
                System.out.println(coche);
            } else {
               System.out.println("Tiene que ser un número entre el 1 y el 4\n");

            }

            if (numero == -1) {
                System.out.println("¿Desea salir?");
                salir = s.nextLine();
                if (salir == "Si"){
                    break;
                }
            }


        } while (numero >= 0 || numero < 4);

















    }
}
