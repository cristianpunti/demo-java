package com.company;

abstract class Vehiculo {

    String tipo;
    int velocidad;


    public String getTipoVehiculo() {

        return tipo;
    };

    public void acelerar() {
        velocidad++;
    }

    public void frenar() {
        velocidad--;
    }

    public int getVelocidad() {
        return velocidad;
    }


}
