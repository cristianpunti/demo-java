package controllers;


import entities.Usuario;
import org.springframework.stereotype.Component;
import services.UsuariosServices;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;


@Component
@Path(value = "/")
public final class UsuariosController {

    private final UsuariosServices usuariosServices;

    public UsuariosController(UsuariosServices usuariosServices) { this.usuariosServices = usuariosServices; }

    @GET
    @Path(value = "/usuarios")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Usuario> listarTodos() {
        return usuariosServices.listarUsuarios();
    }

    @GET
    @Path(value = "/usuarios/{nombre}")
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario listarUno(@PathParam("nombre") String nombre) { return usuariosServices.obtenerUsuario(nombre); }

    @POST
    @Path(value = "/usuarios/{nombre}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response crearUsuario(Usuario usuario) {
        usuariosServices.crearUsuario(usuario);

        return Response.created(
                URI.create("/usuarios/" + usuario.nombreUsuario)
        ).build();
    }

    @DELETE
    @Path(value = "/usuarios/{nombre}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response borrarUsuario(@PathParam("nombre") String nombre) {
        usuariosServices.borrarUsuario(nombre);

        return Response.ok().build();
    }

}
