package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
//
//        Aplica a uno de los proyectos anteriores un patrón de comportamiento de los vistos durante la sesión 9 y explica por qué has escogido ese patrón en concreto.
//
//        Aplica refactoring a cualquiera de los proyectos que se han ido tratando en el curso, recuerda hacerte valer de las técnicas vistas en clase incluidos los patrones de diseño.

        // He escogido el patrón iterador para reducir la duplicidad en el código del recorrido a lo largo de la aplicación
        Usuarios usuarios = new Usuarios();
        usuarios.crear(new Usuario( "Jordi", 48));
        usuarios.crear(new Usuario( "Núria", 39));
        usuarios.crear(new Usuario( "Laura", 33));
        usuarios.crear(new Usuario( "Arcadi", 40));


        anadir(usuarios);
        usuarios.crear(new Usuario( "Núria  de la cadena", 40));
        Usuario usuario = usuarios.siguiente();
        System.out.println("Usuario es: " + usuario.getNombre());

        usuarios.reinicia();
        anadir(usuarios);
    }

    private static void anadir (Usuarios usuarios) {
        while (usuarios.hayMas()){
            Usuario usuario = usuarios.siguiente();
            System.out.println("usuario es:  " + usuario.getNombre());
        }
    }
}
