package com.company;

public interface UsuarioIterator {

    boolean hayMas();

    void reinicia();

    Usuario siguiente();
}
